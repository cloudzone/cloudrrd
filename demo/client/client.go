package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"github.com/parnurzeal/gorequest"
)

type ChartData struct {
	DSName    string  `json:"dsName"`
	AuthKey   string  `json:"authKey"`
	Tag       string  `json:"tag"`
	Value     float64 `json:"value"`
	Timestamp int64   `json:"timestamp"`
}

var (
	r1 *rand.Rand
)

func init() {
	s1 := rand.NewSource(42)
	r1 = rand.New(s1)
}

func random() float64 {

	return r1.Float64() * 1000
}

func main() {
	now := time.Now()
	start := now.AddDate(0, -3, -1)
	tmpSt := start

	for {

		tmpSt = tmpSt.Add(60 * time.Second)
		sTs := tmpSt.Unix()
		d := &ChartData{
			DSName:    "cache",
			AuthKey:   "1f7da224bc12744b4b603f9776c9a52a8",
			Tag:       "flow",
			Value:     random(),
			Timestamp: sTs,
		}
		mJson, _ := json.Marshal(d)

		request := gorequest.New()
		_, body, _ := request.Post("http://127.0.0.1:8800/api/v1/push").Send(string(mJson)).End()
		fmt.Println(sTs, body)

		if sTs > now.Unix() {
			break
		}
		//time.Sleep(time.Second * 1)
		time.Sleep(time.Millisecond * 10)
	}

	fmt.Println(start, now, start.Unix(), now.Unix())
}
