package cloudrrd

import (
	"fmt"
	"time"

	"git.oschina.net/cloudzone/cloudrrd/rrd"
	"github.com/toolkits/file"
)

// 返回最近数据的存储目录路径
func getCurrentDir(dir string) string {
	return fmt.Sprintf("%s/current", dir)
}

// 返回历史数据的存储目录路径
func getHistoryDir(dir string) string {
	return fmt.Sprintf("%s/%s", dir, fromatHistoryPath(time.Now()))
}

// 返回历史数据的存储目录路径
func getHistoryDirByPath(dir, path string) string {
	return fmt.Sprintf("%s/%s", dir, path)
}

// 按时间返回历史数据的存储目录路径
func fromatHistoryPath(t time.Time) string {
	return t.Format("200601")
}

// New 创建并返回cloudrrd.RRD对象，用于操作rrd数据存取
// cfg.Dir 需设置rrd存储路径，默认/tmp/rrd。
func New(cfg Config) *RRD {
	r := &RRD{cfg: cfg}
	r.current = rrd.NewTool().Cfg(&rrd.Config{
		RRDDir: getCurrentDir(r.cfg.Dir),
		DSS: []rrd.DSConfig{
			{
				Name: DSNames.Cache,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			}, {
				Name: DSNames.MQ,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},{
				Name: DSNames.RDS,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},{
				Name: DSNames.OSS,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},{
				Name: DSNames.CDS,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},
		},
	})

	r.history = rrd.NewTool().Cfg(&rrd.Config{
		RRDDir: getHistoryDir(r.cfg.Dir),
		DSS: []rrd.DSConfig{
			{
				Name: DSNames.Cache,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			}, {
				Name: DSNames.MQ,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},{
				Name: DSNames.RDS,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},{
				Name: DSNames.OSS,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},{
				Name: DSNames.CDS,
				Type: rrd.DSTypes.Gauge,
				Step: CloudRRDStepInterval,
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDPDPStep,
						Rows:  cloudRRDPDPStepInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep10m,
						Rows:  cloudRRDCDPStep10mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep30m,
						Rows:  cloudRRDCDPStep30mInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep3h,
						Rows:  cloudRRDCDPStep3hInterval,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Max,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					}, {
						Type:  rrd.RRATypes.Min,
						Xff:   cloudRRDXFF,
						Steps: cloudRRDCDPStep1d,
						Rows:  cloudRRDCDPStep1dInterval,
					},
				},
			},
		},
	})

	return r
}

// Write 保存数据到rrd文件中，数据保存两份（current和history）
// current 保存当前31天数据，history按年月保存数据。
// ds 数据源，包括：DSNames.Cache DSNames.MQ
// authKey tag 为联合主键，保证唯一。
// items 保存数据集。
func (r *RRD) Write(ds, authKey, tag string, items []*rrd.Item) error {
	return r.write(ds, formatPK(authKey, tag), items)
}

func (r *RRD) write(ds, pk string, items []*rrd.Item) error {

	// 按时间分片
	itemsMap := make(map[string][]*rrd.Item)
	for _, item := range items {
		// 时间对齐
		item.Timestamp = alignByStepInterval(item.Timestamp)
		// 取得rrd文件存储的目录
		key := fromatHistoryPath(rrd.Unix(item.Timestamp))
		if itemSli, ok := itemsMap[key]; ok {
			itemSli = append(itemSli, item)
			itemsMap[key] = itemSli
		} else {
			itemSli = make([]*rrd.Item, 1)
			itemSli[0] = item
			itemsMap[key] = itemSli
		}
	}

	// 保存到current目录
	err := r.current.Write(ds, pk, items)
	if err != nil {
		return err
	}

	// 按月保存
	for key, its := range itemsMap {
		dbDir := getHistoryDirByPath(r.cfg.Dir, key)
		err = r.history.WriteByDir(dbDir, ds, pk, its)
		if err != nil {
			return err
		}
	}

	return nil
}

// Update 更新rrd文件中数据
// rraType 查询数据类型：RRATypes.Average RRATypes.Max RRATypes.Min
// ds 数据源，包括：DSNames.Cache DSNames.MQ
// authKey tag 为联合主键。
// items 更新数据集。
func (r *RRD) Update(ds, authKey, tag string, items []*rrd.Item) error {
	return r.update(ds, formatPK(authKey, tag), items)
}

func (r *RRD) update(ds, pk string, items []*rrd.Item) error {
	if len(items) == 0 {
		return nil
	}

	now := time.Now()
	nowTm := now.Unix()
	nowTm = alignByStepInterval(nowTm)
	currentStTm := nowTm - pdpStepTime*daySeconds

	var currentItems []*rrd.Item
	historyItems := make(map[string][]*rrd.Item)
	for _, item := range items {
		if item.Timestamp > currentStTm {
			currentItems = append(currentItems, item)
		}
		subDir := fromatHistoryPath(rrd.Unix(item.Timestamp))
		if history, ok := historyItems[subDir]; ok {
			history = append(history, item)
			historyItems[subDir] = history
		} else {
			history = make([]*rrd.Item, 0)
			history = append(history, item)
			historyItems[subDir] = history
		}
	}

	// update current rrd file
	err := r.current.Update(ds, pk, currentItems)
	if err != nil {
		return err
	}

	// update historytory rrd file
	for subDir, history := range historyItems {
		dbDir := getHistoryDirByPath(r.cfg.Dir, subDir)
		err := r.history.UpdateByDir(dbDir, ds, pk, history)
		if err != nil {
			return err
		}
	}

	return nil
}

// Fetch 从rrd文件中获取数据，查询规则：
// 时间跨度1d，step为1m(60s)
// 时间跨度7d，step为10m(600s)
// 时间跨度30d，step为30m(1800s)
// 时间跨度6month，step为3h(10800s)
// 时间跨度2year，step为1d(86400s)
// 取传入step和计算step的最大值
// rraType 查询数据类型：RRATypes.Average RRATypes.Max RRATypes.Min
// ds 数据源，包括：DSNames.Cache DSNames.MQ
// authKey tag 为联合主键。
// start 开始时间的时间戳 end 结束时间的时间戳
// step 查询的时间跨度。
func (r *RRD) Fetch(rraType, ds, authKey, tag string, start, end int64, step int) (items *rrd.Items, err error) {
	return r.fetch(rraType, ds, formatPK(authKey, tag), start, end, step)
}

// fetch get rrd data from current rrd file
func (r *RRD) fetch(rraType, ds, pk string, start, end int64, step int) (items *rrd.Items, err error) {

	start = alignByStepInterval(start) - CloudRRDStepInterval
	end = alignByStepInterval(end) - CloudRRDStepInterval
	step = getStepByTimeSpan(start, end, step)
	// 原始数据没有max和min采样
	if step == CloudRRDStepInterval {
		rraType = rrd.RRATypes.Average
	}

	// 取得需要的rrd文件与其开始结束时间
	periods, isFromHistory := getRRDFromfile(start, end, step)
	if isFromHistory == false {
		return r.current.Fetch(rraType, ds, pk, rrd.Unix(start), rrd.Unix(end), step)
	}

	// search rrd data from history rrd file
	items = new(rrd.Items)
	for idx, period := range periods {
		dbDir := getHistoryDirByPath(r.cfg.Dir, period.file)
		if !file.IsExist(dbDir) {
			continue
		}

		// 历史目录下查询数据
		itemsRet, err := r.history.FetchByDir(dbDir, rraType, ds, pk, rrd.Unix(period.start), rrd.Unix(period.end), step)
		if err != nil {
			return nil, err
		}
		if itemsRet == nil {
			continue
		}

		// 计算查询结果的最大最小值
		if idx == 0 || items.Max < itemsRet.Max {
			items.Max = itemsRet.Max
		}
		if idx == 0 || items.Min > itemsRet.Min {
			items.Min = itemsRet.Min
		}
		if idx == 0 || items.Step < itemsRet.Step {
			items.Step = itemsRet.Step
		}
		if idx == 0 || items.StartTs > itemsRet.StartTs {
			items.StartTs = itemsRet.StartTs
		}
		items.Size += itemsRet.Size
		items.Data = append(items.Data, itemsRet.Data...)
	}

	return
}

// formatPK  fromat authKey, tag return pk
func formatPK(authKey, tag string) string {
	return fmt.Sprintf("%s%s", authKey, tag)
}

// Get 从rrd文件中获取指定时间点数据
// rraType 查询数据类型：RRATypes.Average RRATypes.Max RRATypes.Min
// ds 数据源，包括：DSNames.Cache DSNames.MQ
// authKey tag 为联合主键。
// ts 查询时间戳
// step 查询的时间跨度。
func (r *RRD) Get(rraType, ds, authKey, tag string, ts int64, step int) (item *rrd.Item, err error) {
	return r.get(rraType, ds, formatPK(authKey, tag), ts, step)
}

func (r *RRD) get(rraType, ds, pk string, ts int64, step int) (item *rrd.Item, err error) {

	// 原始数据没有max和min采样
	if step == CloudRRDStepInterval {
		rraType = rrd.RRATypes.Average
	}

	ts = alignByStepInterval(ts) - CloudRRDStepInterval
	// 取得需要的rrd文件与其开始结束时间
	periods, isFromHistory := getRRDFromfile(ts, ts, step)
	if isFromHistory == false {
		return r.current.Get(rraType, ds, pk, rrd.Unix(ts), step)
	}

	// 没有历史数据
	if len(periods) == 0 {
		return nil, nil
	}

	period := periods[0]
	dbDir := getHistoryDirByPath(r.cfg.Dir, period.file)
	if !file.IsExist(dbDir) {
		return nil, nil
	}

	// 历史目录下查询数据
	return r.history.GetByDir(dbDir, rraType, ds, pk, rrd.Unix(ts), step)
}

// 时间戳对齐
func alignByStepInterval(ts int64) int64 {
	return ts - ts%CloudRRDStepInterval
}

// 根据时间跨度计算step
// 时间跨度1d，step为1m(60s)
// 时间跨度7d，step为10m(600s)
// 时间跨度30d，step为30m(1800s)
// 时间跨度6month，step为3h(10800s)
// 时间跨度2year，step为1d(86400s)
func getStepByTimeSpan(start, end int64, step int) (s int) {

	span := end - start
	s = 86400

	if span <= daySeconds*180 {
		s = 10800
	}

	if span <= daySeconds*30 {
		s = 1800
	}

	if span <= daySeconds*7 {
		s = 600
	}

	if span <= daySeconds {
		s = 60
	}

	if step > s {
		s = step
	}

	return
}
