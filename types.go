package cloudrrd

import "git.oschina.net/cloudzone/cloudrrd/rrd"

// Config cloud rrd config
type Config struct {
	// rrd data store dir, default is /tmp/rrd.
	Dir string
}

// RRD cloud rrd
type RRD struct {
	cfg Config
	//  use current tool by last 31 days rrd data
	current *rrd.Tool
	//  use history tool by ervery month rrd data
	history *rrd.Tool
}

// DSNames cloud data source
var DSNames = struct {
	Cache string
	MQ    string
	RDS   string
	OSS   string
	CDS   string
}{
	"cache",
	"mq",
	"rds",
	"oss",
	"cds",
}

// Tags cloud tags
var Tags = struct {
	Tps     string
	TpsIn   string
	TpsOut  string
	Flow    string
	FlowIn  string
	FlowOut string
}{
	"tps",
	"tpsin",
	"tpsout",
	"flow",
	"flowin",
	"flowout",
}

const (
	daySeconds                 = 86400              // 86400s
	pdpStepTime                = 31                 // 31天
	cloudRRDXFF                = 0.5                // xfiles 因子
	CloudRRDStepInterval       = 60                 // 时间间隔60秒
	cloudRRDPDPStep            = 1                  // 1m
	cloudRRDPDPStepInterval    = 1440 * pdpStepTime // 1分钟原始数据保存31d
	cloudRRDCDPStep10m         = 10                 // 10m
	cloudRRDCDPStep10mInterval = 144 * 90           // 10分钟采样数据保存3month
	cloudRRDCDPStep30m         = 30                 // 30m
	cloudRRDCDPStep30mInterval = 48 * 180           // 30分钟采样数据保存6month
	cloudRRDCDPStep3h          = 180                // 3h
	cloudRRDCDPStep3hInterval  = 8 * 365            // 3小时采样数据保存1year
	cloudRRDCDPStep1d          = 1440               // 1d
	cloudRRDCDPStep1dInterval  = 1 * 365 * 2        // 1天采样数据保存2year
)
