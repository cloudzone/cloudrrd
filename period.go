package cloudrrd

import "time"

type period struct {
	file  string
	start int64
	end   int64
}

// 按时间段和step查询
func getRRDFromfile(start, end int64, step int) ([]*period, bool) {

	now := alignByStepInterval(time.Now().Unix())
	switch step {
	case daySeconds:
		// before 2 year data
		return calPeriods(start, end, now, daySeconds*365*2, daySeconds*28)
	case 10800:
		// before 1 year data
		return calPeriods(start, end, now, daySeconds*365, daySeconds*28)
	case 1800:
		// before 6 month data
		return calPeriods(start, end, now, daySeconds*180, daySeconds*28)
	case 600:
		// before 3 month data
		return calPeriods(start, end, now, daySeconds*90, daySeconds*28)
	case 60:
		// before 1 month data
		return calPeriods(start, end, now, daySeconds*30, daySeconds*28)
	default:
	}

	return nil, false
}

// 按max和interval将开始和结束划分为时间段，max是最大可查询时间区间，interval是时间区间
func calPeriods(start, end, now, max, interval int64) (ps []*period, isHis bool) {

	if now < end {
		// will not happen unless test data
		now = end
	}

	if now-start <= max {
		return nil, false
	}

	st := start
	for {
		pd := new(period)
		pd.file = fromatHistoryPath(time.Unix(st, 0))

		sTm := time.Unix(st, 0)
		if st == start {
			// 文件的开始时间为查询开始时间
			pd.start = st
		} else {
			// 文件的开始时间为本月第一天
			nt := firstDayFromMonth(sTm)
			pd.start = nt.Unix()
		}

		eTm := lastDayFromMonth(sTm)
		et := eTm.Unix()
		if et > end {
			// 文件的结束时间为查询结束时间
			pd.end = end
		} else {
			// 文件的结束时间为本月最后一天
			pd.end = et
		}

		st += interval
		ps = append(ps, pd)
		if end-st <= 0 {
			ef := fromatHistoryPath(time.Unix(end, 0))
			if pd.file != ef {

				pd = new(period)
				pd.file = ef
				sTm := time.Unix(st, 0)
				if st == start {
					pd.start = st
				} else {
					nt := firstDayFromMonth(sTm)
					pd.start = nt.Unix()
				}

				eTm := lastDayFromMonth(sTm)
				et := eTm.Unix()
				if et > end {
					pd.end = end
				} else {
					pd.end = et
				}
				ps = append(ps, pd)
			}
			break
		}
	}

	return ps, true
}

// 当月第一天
func firstDayFromMonth(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, t.Location())
}

// 当月最后一天
func lastDayFromMonth(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 23, 59, 59, 0, t.Location()).AddDate(0, 1, -1)
}

func firstTimeFromDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
}

func lastTimeFromDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 23, 59, 59, 0, t.Location())
}

// 格式化日期字符串，返回单月第一天和最后一天时间戳
func parseStartAndEndUnix(d string) (int64, int64, error) {
	s, err := time.Parse("200601", d)
	if err != nil {
		return 0, 0, err
	}
	e := lastDayFromMonth(s)

	return s.Unix(), e.Unix(), nil
}
