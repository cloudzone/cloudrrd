package main

import (
	"fmt"
	"time"

	"git.oschina.net/cloudzone/cloudrrd/rrd"
)

const (
	pdprows = 10
)

func main() {

	tool := rrd.NewTool().Cfg(&rrd.Config{
		RRDDir: "./data",
		DSS: []rrd.DSConfig{
			{
				Name: "test",
				Type: rrd.DSTypes.Gauge,
				Step: 60,
				Min:  "0",
				Max:  "1000",
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Steps: 1,
						Rows:  pdprows,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   0.5,
						Steps: 5,
						Rows:  576,
					},
				},
			},
		},
	})

	fmt.Println("write rrd data:")
	var items []*rrd.Item
	//now := time.Unix(1494211920, 0)
	now := time.Now()
	nowTm := now.Unix()
	nowTm = nowTm - nowTm%60
	now = time.Unix(nowTm, 0)
	for i := 0; i < 2; i++ {
		item := new(rrd.Item)
		item.Value = rrd.JSONFloat(i)
		item.Timestamp = now.Add(time.Duration(i*60) * time.Second).Unix()
		fmt.Println("==>", item.Value, item.Timestamp)
		items = append(items, item)
	}
	err := tool.Write("test", "1", items)
	if err != nil {
		panic(err)
	}

	end := now.Add(time.Duration(pdprows*60) * time.Second)
	startTs := now.Unix()
	endTs := end.Unix()
	startTs = startTs - startTs%int64(60) - int64(60)
	endTs = endTs - endTs%int64(60)

	itemsRet, err := tool.Fetch(rrd.RRATypes.Average, "test", "1", rrd.Unix(startTs), rrd.Unix(endTs), 60)
	if err != nil {
		panic(err)
	}

	for idx, v := range itemsRet.Data {
		fmt.Println(idx, "->", v)
	}

	fmt.Println("\nupdate rrd data:")
	items = []*rrd.Item{}
	for i := 0; i < 2; i++ {
		item := new(rrd.Item)
		item.Value = rrd.JSONFloat(i + 100)
		item.Timestamp = now.Add(time.Duration(i*60) * time.Second).Unix()
		fmt.Println("==>", item.Value, item.Timestamp)
		items = append(items, item)
	}
	err = tool.Update("test", "1", items)
	if err != nil {
		panic(err)
	}

	itemsRet, err = tool.Fetch(rrd.RRATypes.Average, "test", "1", rrd.Unix(startTs), rrd.Unix(endTs), 60)
	if err != nil {
		panic(err)
	}

	for idx, v := range itemsRet.Data {
		fmt.Println(idx, "->", v)
	}

	fmt.Println("ok", now, time.Unix(endTs, 0), startTs, endTs)
}
