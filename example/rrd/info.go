package main

import (
	"fmt"

	"git.oschina.net/cloudzone/cloudrrd/rrd"
)

const (
	pdprows = 10
)

func main() {

	tool := rrd.NewTool().Cfg(&rrd.Config{
		RRDDir: "./data",
		DSS: []rrd.DSConfig{
			{
				Name: "test",
				Type: rrd.DSTypes.Gauge,
				Step: 60,
				Min:  "0",
				Max:  "1000",
				RRAS: []rrd.RRAConfig{
					{
						Type:  rrd.RRATypes.Average,
						Steps: 1,
						Rows:  pdprows,
					}, {
						Type:  rrd.RRATypes.Average,
						Xff:   0.5,
						Steps: 5,
						Rows:  576,
					},
				},
			},
		},
	})

	fi, err := tool.Info("./data", "test", "1")
	if err != nil {
		panic(err)
	}

	fmt.Println("ok", fi)
}
