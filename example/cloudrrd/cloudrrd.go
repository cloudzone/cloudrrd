package main

import (
	"fmt"
	"time"

	"git.oschina.net/cloudzone/cloudrrd"
	"git.oschina.net/cloudzone/cloudrrd/rrd"
)

const (
	pdprows = 10
)

func main() {
	rd := cloudrrd.New(cloudrrd.Config{Dir: "./data"})

	var items []*rrd.Item
	now := time.Now()
	tmp := now.Unix()
	tmp = tmp - tmp%60
	now = time.Unix(tmp, 0)
	fmt.Println("write rrd data :")
	for i := 0; i < pdprows; i++ {
		item := new(rrd.Item)
		item.Value = rrd.JSONFloat(i)
		item.Timestamp = now.Add(time.Duration(i*60) * time.Second).Unix()
		fmt.Println("\t", item)
		items = append(items, item)
	}
	err := rd.Write(cloudrrd.DSNames.Cache, "key", "tps", items)
	if err != nil {
		panic(err)
	}

	end := now.Add(time.Duration((pdprows-1)*60) * time.Second)
	startTs := now.Unix()
	endTs := end.Unix()
	startTs = startTs - startTs%int64(60)
	endTs = endTs - endTs%int64(60)
	itemsRet, err := rd.Fetch(rrd.RRATypes.Average, cloudrrd.DSNames.Cache, "key", "tps", startTs, endTs, 60)
	if err != nil {
		panic(err)
	}

	fmt.Printf("query write rrd data start[%d] end[%d] :\n", startTs, endTs)
	for _, v := range itemsRet.Data {
		fmt.Println("\t", v)
	}

	itemRet, err := rd.Get(rrd.RRATypes.Average, cloudrrd.DSNames.Cache, "key", "tps", startTs, 60)
	if err != nil {
		panic(err)
	}
	fmt.Println("get rrd data :", startTs, itemRet)

	fmt.Println("update rrd data :")
	for i, item := range items {
		item.Value = rrd.JSONFloat(i + 100)
		fmt.Println("\t", item)
	}

	err = rd.Update(cloudrrd.DSNames.Cache, "key", "tps", items)
	if err != nil {
		panic(err)
	}

	fmt.Printf("query update rrd data start[%d] end[%d] :\n", startTs, endTs)
	itemsRet, err = rd.Fetch(rrd.RRATypes.Average, cloudrrd.DSNames.Cache, "key", "tps", startTs, endTs, 60)
	if err != nil {
		panic(err)
	}

	for _, v := range itemsRet.Data {
		fmt.Println("\t", v)
	}

	fmt.Println("ok", now, time.Unix(endTs, 0))
}
