package rrd

import (
	"fmt"
	"math"
	"sync"
	"time"

	"github.com/gunsluo/rrdlite"
	"github.com/toolkits/file"
)

// Tool rrd tool
type Tool struct {
	rrdDir  string
	startTs int64
	dss     map[string]*DSConfig
	lock    sync.Mutex
	fLocks  map[string]*sync.RWMutex
}

// NewTool return new rrd tool
func NewTool() *Tool {
	return &Tool{
		rrdDir:  "/tmp/rrd",
		startTs: time.Now().Unix() - 86400,
		fLocks:  make(map[string]*sync.RWMutex),
	}
}

// Cfg config rrd tool
func (t *Tool) Cfg(cfg *Config) *Tool {
	if cfg.RRDDir != "" {
		t.rrdDir = cfg.RRDDir
	}

	if cfg.StartTs != 0 {
		t.startTs = cfg.StartTs
	}

	t.dss = make(map[string]*DSConfig, len(cfg.DSS))
	for _, ds := range cfg.DSS {
		d := new(DSConfig)
		d.Name = ds.Name
		d.Type = ds.Type
		if ds.Step == 0 {
			d.Step = 60
		} else {
			d.Step = ds.Step
		}
		if ds.Heartbeat == 0 {
			d.Heartbeat = d.Step * 2
		} else {
			d.Heartbeat = d.Heartbeat
		}
		if ds.Min == "" {
			d.Min = "U"
		} else {
			d.Min = ds.Min
		}
		if ds.Max == "" {
			d.Max = "U"
		} else {
			d.Max = ds.Max
		}

		for _, rra := range ds.RRAS {
			r := new(RRAConfig)
			r.Type = rra.Type
			if rra.Xff == 0 {
				r.Xff = 0.5
			} else {
				r.Xff = rra.Xff
			}
			r.Steps = rra.Steps
			r.Rows = rra.Rows
			d.RRAS = append(d.RRAS, *r)
		}

		t.dss[ds.Name] = d
	}

	return t
}

// RRDDir set rrd dir
func (t *Tool) RRDDir(dir string) *Tool {

	if dir != "" {
		t.rrdDir = dir
	}
	return t
}

func (t *Tool) create(ds, dbfile string, start time.Time) error {

	d, ok := t.dss[ds]
	if !ok {
		return fmt.Errorf("no config data source[%s]", ds)
	}

	c := rrdlite.NewCreator(dbfile, start, d.Step)
	c.DS(d.Name, d.Type, d.Heartbeat, d.Min, d.Max)

	for _, rra := range d.RRAS {
		c.RRA(rra.Type, rra.Xff, rra.Steps, rra.Rows)
	}

	return c.Create(true)
}

// Write 保存数据到current目录
func (t *Tool) Write(ds, pk string, items []*Item) error {
	return t.WriteByDir(t.rrdDir, ds, pk, items)
}

// WriteByDir 保存数据到指定的目录
func (t *Tool) WriteByDir(dir, ds, pk string, items []*Item) error {
	return t.write(dir, ds, pk, items, false)
}

// ReWrite 重写数据到current目录，原数据会丢失。
func (t *Tool) ReWrite(ds, pk string, items []*Item) error {
	return t.ReWriteByDir(t.rrdDir, ds, pk, items)
}

// ReWriteByDir 重写数据到current目录，原数据会丢失。
func (t *Tool) ReWriteByDir(dir, ds, pk string, items []*Item) error {
	return t.write(dir, ds, pk, items, true)
}

func (t *Tool) write(dir, ds, pk string, items []*Item, rewrite bool) error {
	if len(items) == 0 {
		return nil
	}

	d, ok := t.dss[ds]
	if !ok {
		return fmt.Errorf("no config data source[%s]", ds)
	}

	// stroe file name
	dbfile := formatDBfile(dir, ds, d.Type, pk)

	found := file.IsExist(dbfile)
	if rewrite == true || !found {
		// 目录是否存在。
		baseDir := file.Dir(dbfile)
		err := file.InsureDir(baseDir)
		if err != nil {
			return err
		}

		// 创建新文件用于保存数据。
		rw := t.getLockByFile(dbfile)
		rw.Lock()
		err = t.create(ds, dbfile, Unix(t.startTs))
		rw.Unlock()
		if err != nil {
			return err
		}
	}

	// update data into rrd file
	u := rrdlite.NewUpdater(dbfile)
	for idx, item := range items {
		// 新文件增加一个开始0数据
		if (rewrite == true || !found) && idx == 0 {
			u.Cache(item.Timestamp-int64(d.Step), 0)
		}
		v := math.Abs(float64(item.Value))
		if v > 1e+300 || (v < 1e-300 && v > 0) {
			continue
		}
		if d.Type == DSTypes.Derive || d.Type == DSTypes.Counter {
			u.Cache(item.Timestamp, int(item.Value))
		} else {
			u.Cache(item.Timestamp, item.Value)
		}
	}

	rw := t.getLockByFile(dbfile)
	rw.Lock()
	err := u.Update()
	rw.Unlock()

	return err
}

// Update 更新数据
func (t *Tool) Update(ds, pk string, items []*Item) error {
	return t.UpdateByDir(t.rrdDir, ds, pk, items)
}

// Update 更新指定目录文件的数据
func (t *Tool) UpdateByDir(dir, ds, pk string, items []*Item) error {
	return t.update(dir, ds, pk, items)
}

func (t *Tool) update(dir, ds, pk string, items []*Item) error {
	if len(items) == 0 {
		return nil
	}

	d, ok := t.dss[ds]
	if !ok {
		return fmt.Errorf("no config data source[%s]", ds)
	}

	// stroe file name
	dbfile := formatDBfile(dir, ds, d.Type, pk)

	found := file.IsExist(dbfile)
	if !found {
		return fmt.Errorf("data source[%s] pk[%s] data not found", ds, pk)
	}

	// update his data into rrd file
	var minTm int64
	u := rrdlite.NewUpdater(dbfile)
	for _, item := range items {
		v := math.Abs(float64(item.Value))
		if v > 1e+300 || (v < 1e-300 && v > 0) {
			continue
		}
		if d.Type == DSTypes.Derive || d.Type == DSTypes.Counter {
			u.Cache(item.Timestamp, int(item.Value))
		} else {
			u.Cache(item.Timestamp, item.Value)
		}

		if item.Timestamp < minTm || minTm == 0 {
			minTm = item.Timestamp
		}
	}
	u.SetStartUpdateTm(minTm - int64(d.Step))

	rw := t.getLockByFile(dbfile)
	rw.Lock()
	err := u.Update()
	rw.Unlock()

	return err
}

// Fetch 查询数据
// ds 数据源，根据业务场景指定。
// key 主键，保证唯一。
// start 开始时间 end 结束时间
// step 查询的时间跨度。
func (t *Tool) Fetch(rraType, ds, pk string, start, end time.Time, step int) (items *Items, err error) {
	return t.FetchByDir(t.rrdDir, rraType, ds, pk, start, end, step)
}

// FetchByDir 指定目录查询数据
// ds 数据源，根据业务场景指定。
// key 主键，保证唯一。
// start 开始时间 end 结束时间
// step 查询的时间跨度。
func (t *Tool) FetchByDir(dir, rraType, ds, pk string, start, end time.Time, step int) (items *Items, err error) {

	// 取得配置
	d, ok := t.dss[ds]
	if !ok {
		return nil, fmt.Errorf("no config data source[%s]", ds)
	}

	dbfile := formatDBfile(dir, ds, d.Type, pk)
	if !file.IsExist(dbfile) {
		//return nil, fmt.Errorf("file[%s] no exist", dbfile)
		return nil, nil
	}

	// rrd file 中取数据
	stepDt := time.Duration(step) * time.Second
	rw := t.getLockByFile(dbfile)
	rw.RLock()
	fetchRes, err := rrdlite.Fetch(dbfile, rraType, start, end, stepDt)
	rw.RUnlock()
	if err != nil {
		return &Items{}, err
	}
	defer fetchRes.FreeValues()

	values := fetchRes.Values()
	items = new(Items)
	items.StartTs = fetchRes.Start.Unix()
	items.Step = fetchRes.Step.Seconds()
	size := len(values)

	// 组织返回数据并计算最大最小值。
	var max float64
	var min float64
	fstNumIdx := -1
	endTs := end.Unix()
	for i, val := range values {
		ts := items.StartTs + int64(i+1)*int64(items.Step)
		if i == size-1 && endTs < ts {
			continue
		}

		d := &Item{
			Timestamp: ts,
			Value:     JSONFloat(val),
		}
		items.Data = append(items.Data, d)

		if math.IsNaN(val) {
			continue
		}

		// 最大最小值计算
		if fstNumIdx == -1 {
			fstNumIdx = i
		}

		if val > max || fstNumIdx == i {
			max = val
		}

		if val < min || fstNumIdx == i {
			min = val
		}
	}
	items.Size = len(items.Data)
	items.Max = JSONFloat(max)
	items.Min = JSONFloat(min)

	return items, nil
}

// Get get data from rrd file
func (t *Tool) Get(rraType, ds, pk string, tm time.Time, step int) (item *Item, err error) {
	return t.GetByDir(t.rrdDir, rraType, ds, pk, tm, step)
}

// GetByDir get data from rrd file
func (t *Tool) GetByDir(dir, rraType, ds, pk string, tm time.Time, step int) (item *Item, err error) {

	items, err := t.FetchByDir(dir, rraType, ds, pk, tm, tm, step)
	if err != nil {
		return nil, err
	}

	if items.Size == 0 {
		return nil, err
	}

	return items.Data[0], nil
}

// Info return rrd file info
func (t *Tool) Info(dir, ds, pk string) (fi *FileInfo, err error) {

	d, ok := t.dss[ds]
	if !ok {
		return nil, fmt.Errorf("no config data source[%s]", ds)
	}

	dbfile := formatDBfile(dir, ds, d.Type, pk)

	info, err := rrdlite.Info(dbfile)
	if err != nil {
		return nil, err
	}

	fi = new(FileInfo)
	if v, ok := info["rrd_version"]; ok {
		if val, ok := v.(string); ok {
			fi.Version = val
		}
	}

	if v, ok := info["last_update"]; ok {
		if val, ok := v.(uint); ok {
			fi.LastUpdate = int64(val)
		}
	}

	if v, ok := info["ds.index"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsIndex = make(map[string]uint)
			for k, vv := range val {
				if vval, ok := vv.(uint); ok {
					fi.DsIndex[k] = vval
				}
			}
		}
	}

	if v, ok := info["ds.max"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsMax = make(map[string]float64)
			for k, vv := range val {
				if vval, ok := vv.(float64); ok {
					fi.DsMax[k] = vval
				}
			}
		}
	}

	if v, ok := info["ds.min"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsMin = make(map[string]float64)
			for k, vv := range val {
				if vval, ok := vv.(float64); ok {
					fi.DsMin[k] = vval
				}
			}
		}
	}

	if v, ok := info["ds.last_ds"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsLastDs = make(map[string]string)
			for k, vv := range val {
				if vval, ok := vv.(string); ok {
					fi.DsLastDs[k] = vval
				}
			}
		}
	}

	if v, ok := info["ds.unknown_sec"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsUnknownSec = make(map[string]uint)
			for k, vv := range val {
				if vval, ok := vv.(uint); ok {
					fi.DsUnknownSec[k] = vval
				}
			}
		}
	}

	if v, ok := info["ds.type"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsType = make(map[string]string)
			for k, vv := range val {
				if vval, ok := vv.(string); ok {
					fi.DsType[k] = vval
				}
			}
		}
	}

	if v, ok := info["ds.minimal_heartbeat"]; ok {
		if val, ok := v.(map[string]interface{}); ok {
			fi.DsHeartbeat = make(map[string]uint)
			for k, vv := range val {
				if vval, ok := vv.(uint); ok {
					fi.DsHeartbeat[k] = vval
				}
			}
		}
	}

	if v, ok := info["rra.cf"]; ok {
		if val, ok := v.([]interface{}); ok {
			for _, vv := range val {
				if vval, ok := vv.(string); ok {
					fi.RRACfs = append(fi.RRACfs, vval)
				}
			}
		}
	}

	if v, ok := info["rra.rows"]; ok {
		if val, ok := v.([]interface{}); ok {
			for _, vv := range val {
				if vval, ok := vv.(uint); ok {
					fi.RRARows = append(fi.RRARows, vval)
				}
			}
		}
	}

	if v, ok := info["rra.xff"]; ok {
		if val, ok := v.([]interface{}); ok {
			for _, vv := range val {
				if vval, ok := vv.(float64); ok {
					fi.RRAXff = append(fi.RRAXff, vval)
				}
			}
		}
	}

	if v, ok := info["rra.pdp_per_row"]; ok {
		if val, ok := v.([]interface{}); ok {
			for _, vv := range val {
				if vval, ok := vv.(uint); ok {
					fi.RRAPdpPerRow = append(fi.RRAPdpPerRow, vval)
				}
			}
		}
	}

	if v, ok := info["step"]; ok {
		if val, ok := v.(uint); ok {
			fi.Step = val
		}
	}

	if v, ok := info["header_size"]; ok {
		if val, ok := v.(uint); ok {
			fi.HeaderSize = val
		}
	}

	return
}

func (t *Tool) getLockByFile(file string) *sync.RWMutex {

	t.lock.Lock()
	rw, ok := t.fLocks[file]
	if !ok {
		rw = new(sync.RWMutex)
		t.fLocks[file] = rw
	}
	t.lock.Unlock()

	return rw
}
