package rrd

// Item rrd item data
type Item struct {
	Value     JSONFloat `json:"value"`
	Timestamp int64     `json:"timestamp"`
}

// Items rrd items
type Items struct {
	Max     JSONFloat `json:"max"`
	Min     JSONFloat `json:"min"`
	Size    int       `json:"size"`
	Step    float64   `json:"step"`
	StartTs int64     `json:"startTs"`
	Data    []*Item   `json:"data"`
}
