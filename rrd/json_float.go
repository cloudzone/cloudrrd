package rrd

import (
	"fmt"
	"math"
)

// JSONFloat float64 type json by rrd
type JSONFloat float64

// MarshalJSON json marshalJson
func (v JSONFloat) MarshalJSON() ([]byte, error) {
	f := float64(v)
	if v.IsNaN() {
		return []byte("null"), nil
	}

	return []byte(fmt.Sprintf("%f", f)), nil
}

// IsNaN return bool
func (v JSONFloat) IsNaN() bool {
	f := float64(v)
	return math.IsNaN(f) || math.IsInf(f, 0)
}

// Value return float64
func (v JSONFloat) Value() float64 {
	if v.IsNaN() {
		return 0
	}
	return float64(v)
}

// Int64 return int64
func (v JSONFloat) Int64() int64 {
	if v.IsNaN() {
		return 0
	}
	return int64(v)
}
