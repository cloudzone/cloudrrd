package rrd

// FileInfo rrd file info
type FileInfo struct {
	Version      string
	LastUpdate   int64
	DsIndex      map[string]uint
	DsMax        map[string]float64
	DsMin        map[string]float64
	DsLastDs     map[string]string
	DsUnknownSec map[string]uint
	DsType       map[string]string
	DsHeartbeat  map[string]uint
	RRACfs       []string
	RRARows      []uint
	RRAXff       []float64
	RRAPdpPerRow []uint
	Step         uint
	HeaderSize   uint
}
